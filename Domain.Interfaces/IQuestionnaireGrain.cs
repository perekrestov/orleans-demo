﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IQuestionnaireGrain : Orleans.IGrainWithIntegerKey
    {
        Task Set(Guid key, object value);
        Task Remove(Guid key);
        Task<object> GetAnswer(Guid key);
        Task<int> GetCount();
        Task<Dictionary<Guid, object>> GetSnapshot();
    }
}