﻿namespace Domain.Interfaces
{
    public class ClusterOptionConstants
    {
        public static readonly string ClusterId = "orleans-demo-cluster";
        public static readonly int DefaultSiloPort = 11111;
        public static readonly int DefaultGatewayPort = 30000;
    }
}