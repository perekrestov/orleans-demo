﻿using System.Collections.Immutable;
using System.Threading.Tasks;
using Orleans;

namespace Domain.Interfaces
{
    public interface IQuestionnaireListGrain : IGrainWithIntegerKey
    {
        Task Add(IQuestionnaireGrain q);
        Task<int> GetMaxId();
        Task<ImmutableArray<int>> GetAll();
    }
}