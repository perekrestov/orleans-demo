﻿namespace Domain.Interfaces
{
    public enum PrequalificationStatus
    {
        Accepted,
        Declined,
        Underwriting,
    }
}