﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Orleans;

namespace Domain.Interfaces
{
    public interface IQuestionnaireManagerGrain : IGrainWithIntegerKey
    {
        Task Set(Guid key, object value);
        Task Remove(Guid key);
        Task<Dictionary<Guid, object>> GetSnapshot();
        Task<object> GetAnswer(Guid controlId);
    }
}