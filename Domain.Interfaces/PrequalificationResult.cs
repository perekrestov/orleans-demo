﻿namespace Domain.Interfaces
{
    public class PrequalificationResult
    {
        public PrequalificationStatus Status { get; set; }
    }
}