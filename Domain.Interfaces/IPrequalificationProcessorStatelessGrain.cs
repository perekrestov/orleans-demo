﻿using System.Threading.Tasks;
using Orleans;

namespace Domain.Interfaces
{
    public interface IPrequalificationProcessorStatelessGrain : IGrainWithIntegerKey
    {
        Task<PrequalificationResult> Evaluate(IQuestionnaireGrain questionnaireGrain);
    }
}