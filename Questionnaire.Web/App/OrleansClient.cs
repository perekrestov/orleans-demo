﻿using System;
using System.Threading.Tasks;
using Domain.Interfaces;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Clustering.Kubernetes;
using Orleans.Configuration;
using Orleans.Runtime;

namespace Client
{
    public class OrleansClient
    {
        private readonly ILogger _logger;

        public OrleansClient(ILogger<OrleansClient> logger)
        {
            _logger = logger;
        }


        public async Task<IClusterClient> StartClientWithRetries(bool useKubernates,
            int initializeAttemptsBeforeFailing = 5)
        {
            int attempt = 0;
            IClusterClient client;
            while (true)
            {
                try
                {
                    client = new ClientBuilder()
                        .Configure<ClusterOptions>(options => options.ClusterId = ClusterOptionConstants.ClusterId)
                        .Clustering(useKubernates)
                        .ConfigureLogging(logging => logging.AddConsole())
                        .Build();

                    await client.Connect();
                    _logger.Info("Client successfully connect to silo host");
                    break;
                }
                catch (SiloUnavailableException)
                {
                    attempt++;
                    _logger.Info(
                        $"Attempt {attempt} of {initializeAttemptsBeforeFailing} failed to initialize the Orleans client.");
                    if (attempt > initializeAttemptsBeforeFailing)
                    {
                        throw;
                    }

                    await Task.Delay(TimeSpan.FromSeconds(4));
                }
            }

            return client;
        }
    }

    static class ClusteringConfigurationExtensions
    {
        public static IClientBuilder Clustering(this IClientBuilder builder, bool useKubernetes)
        {
            if (useKubernetes)
            {
                return builder.UseKubeGatewayListProvider();
            }

            return builder.UseLocalhostClustering();
        }
    }
}