﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using Questionnaire.Web.Controllers;

namespace Questionnaire.Web.Models
{
    public class ApplicationModel
    {
        public int Id { get; set; }

        public Guid ControlId { get; set; }

        public string Value { get; set; }

        public string Snapshot { get; set; }

        public List<SelectListItem> Controls { get; } = ControlController.Controls
            .OrderBy(t => t.Value).Select(t => new SelectListItem {Value = t.Key.ToString(), Text = t.Value}).ToList();
    }
}