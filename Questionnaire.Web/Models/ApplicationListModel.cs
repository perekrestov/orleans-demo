﻿using System.Collections.Generic;

namespace Questionnaire.Web.Models
{
    public class ApplicationListModel
    {
        public IReadOnlyList<int> Applications { get; set; }
    }
}