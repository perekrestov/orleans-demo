﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Runtime;

namespace Questionnaire.Web.Controllers
{
    [Route("api/questionnaire-generator")]
    public class QuestionnaireGeneratorController
    {
        private readonly IClusterClient _client;
        private readonly ILogger<QuestionnaireGeneratorController> _logger;

        public QuestionnaireGeneratorController(IClusterClient client, ILogger<QuestionnaireGeneratorController> logger)
        {
            _client = client;
            _logger = logger;
        }

        [HttpPost, Route("{count}")]
        public async Task Post(int count)
        {
            var listGrain = _client.GetGrain<IQuestionnaireListGrain>(0);
            var maxId = await listGrain.GetMaxId();
            var tasks = Enumerable.Range(maxId + 1, count).Select(async id =>
            {
                var grain = _client.GetGrain<IQuestionnaireManagerGrain>(id);
                await grain.Set(Guid.Empty, "Grain - " + id);
            });

            await Task.WhenAll(tasks);
            _logger.Info($"{count} applications generated.");
        }
    }
}