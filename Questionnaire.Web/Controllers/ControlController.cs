﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace Questionnaire.Web.Controllers
{
    [Route("api/[controller]")]
    public class ControlController
    {
        public static readonly Dictionary<Guid, string> Controls = new Dictionary<Guid, string>
        {
            [Guid.Parse("485821c4-20f6-4afb-8727-57bf7bef3135")] = "SSN",
            [Guid.Parse("485821c4-20f6-4afb-8727-57bf7bef31d1")] =
                "Any applicant convicted of insurance fraud (ineligible) or a Felony (referral)? - If yes, explain:",
            [Guid.Parse("6182b15e-93a3-42f0-85b9-ca53aefa3575")] = "Sport Facility?",
            [Guid.Parse("a305d834-f892-4e39-8b0d-1a4c980dbb5f")] =
                "Is there a safe in the residence? - If yes, explain:",
            [Guid.Parse("9966db60-e62c-46df-9abb-2071cac513c6")] =
                "Does the applicant operate as a fee auditing service for others and/or are employees required to travel as part of normal job duties? ",
            [Guid.Parse("43cfb355-afa7-4342-8f04-4b332ac45491")] =
                "Does the insured subcontract all of the erection of forms to others? ",
            [Guid.Parse("6b3b94f2-8736-4dbd-9768-4d233d193b6b")] =
                "If a fuel tank is on premises, has other insurance been obtained for the tank?",
            [Guid.Parse("fa713188-5095-490e-af27-66b9027c6f3c")] =
                "Does the insured understand volunteers are not employees per the CA Labor Code and are not afforded coverage?",
            [Guid.Parse("2da4cfbd-39be-4047-9f04-0aa7fe1096b2")] = "Does customer sign off on adjustments?",
            [Guid.Parse("aeb80b44-efe2-4c1d-90a0-5112f200a766")] =
                "Social or Human Services - Adult day care? - Is their primary operation an adult day care?",
            [Guid.Parse("f05c48eb-0351-45b7-b0d0-a222556b51bc")] = "Billing Contact Phone:",
            [Guid.Parse("0a3aa764-c808-4833-8488-c83586d9230e")] = "Any \"live in\" care provided?",
            [Guid.Parse("abc086d3-c457-43f6-a8d1-80a92f3153bd")] = "Street & Road Surveyors?",
            [Guid.Parse("f91d3725-8ae4-48e1-8768-f3c0b9953d47")] =
                "Types of projects undertaken - Industrial Waste Treatment:",
            [Guid.Parse("fd5858a8-3953-43dc-bba3-69991b6b0032")] =
                "Does the applicant operate as a discount paint shop? ",
            [Guid.Parse("2dc63e21-4661-4ca2-b4cb-74fdebbf5a07")] = "Educational - Language or cultural school?",
            [Guid.Parse("d9b056e2-ce1c-401d-ad01-062d63fc8636")] =
                "Non-Structural Iron, Brass or Steel Erection (Interior)?",
            [Guid.Parse("5a1e1383-7f75-48ea-b1b4-ff9fab22ad5b")] = "Approximate date of last loss control visit:",
            [Guid.Parse("01e054ae-df5d-4b5d-b059-8752ba6f79d1")] = "Does the insured perform any vermiculite work?",
            [Guid.Parse("83cf8949-6cd5-408b-a0ff-9846baddd2df")] =
                "Environmentally Impacted Sites? - Revenue to the firm:",
            [Guid.Parse("d4798380-41f6-4877-a9cc-671d8bd0cd43")] = "Acceptable underlying carrier (B+ V or better)?",
            [Guid.Parse("7a131635-ca57-4850-af01-89370d81fe6a")] =
                "The insured must use a contract that contains a limitation of damages provision. Is the liquidated damages clause ever waived?",
            [Guid.Parse("7fe2ce40-4a69-4449-ac82-349277a5a78b")] = "Rheumatology?",
            [Guid.Parse("3040a769-e018-454c-81aa-2611de60d765")] =
                "Does Applicant's website(s) advertise services or products other than the Applicant's own? - If yes, explain:",
            [Guid.Parse("e14f227a-6846-44a7-84c5-f2a97f6bd370")] = "Percentage of work / exposure - 12 - 24 feet:",
            [Guid.Parse("5177b43e-f203-4316-bcb2-af18b3a246a3")] =
                "Describe current year general aggregate limit other than applies per policy, location and project:",
            [Guid.Parse("4525a52f-d6d9-4db6-b5b7-1c2dda82ab80")] =
                "Does the insured do any work outside of the state he/she is domiciled in?",
            [Guid.Parse("903d149f-02e1-42ff-af8d-e498a4f5c23d")] =
                "Final signoff of completed job by - General contractor?",
            [Guid.Parse("63c7ed2b-6ac9-4c12-9c37-9c68925d9877")] = "Hours worked per week:",
            [Guid.Parse("9843b89a-d24b-472c-8d4b-687538232a22")] =
                "Does the applicant provide services to residential customers? ",
            [Guid.Parse("8ff57ea5-8582-4f99-af9e-4a143db592aa")] =
                "Optometrists Liability? - Has the professional liability insurance for the applicant ever been cancelled or non-renewed? ",
            [Guid.Parse("4c82bba5-3bc6-444d-8e5d-6421f6984a1e")] = "Medical or Health - Emergency medical services?",
            [Guid.Parse("ee188897-ea7a-42b1-a649-97c7d8d3e1b9")] =
                "Direct bill options - 10 Pay (20% Down + Fees & 9 Payments)?",
            [Guid.Parse("5901e1ed-ed27-4eec-ad08-e5d7410fb58b")] = "Does the insured hire or rent trucks?",
            [Guid.Parse("4bcecb54-a03e-4b43-9871-d1e0579fe5db")] = "Other field crops - General Farms, Primarily Crop?",
            [Guid.Parse("eec63542-09e0-4c0b-a925-4a9a5fbe0c81")] = "Target premium range - Umbrella:",
            [Guid.Parse("15979491-6505-4c17-b05d-ec383ae457db")] = "Legal Entity - Incorporated Labor Union",
            [Guid.Parse("22204b27-fd93-4930-98af-0c1ec01e43cc")] = "Social or Human Services - Counseling services?",
            [Guid.Parse("83d69b2b-8d10-4674-a664-2b23af95e013")] =
                "Does the Insured advise clients to invest in any enterprise in which the insured or a member of the Insured's firm has an ownership interest?",
            [Guid.Parse("320e8e34-e19a-4c4b-92fe-ae73ae83d0cc")] = "Legal Entity - General Partnership",
            [Guid.Parse("cffeeb2a-bec5-43d4-bd0b-58d2fb61db67")] =
                "Are individuals other than clergy involved with counseling? - If yes, list credentials or training required prior to counseling:",
        };

        [HttpGet]
        public Dictionary<Guid, string> Get()
        {
            return Controls;
        }
    }
}