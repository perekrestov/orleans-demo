﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Orleans;

namespace Questionnaire.Web.Controllers
{
    [Route("api/questionnaire-shapshot")]
    public class QuestionnaireShapshotController : ControllerBase
    {
        private readonly IClusterClient _client;
        private readonly ILogger<QuestionnaireShapshotController> _logger;

        public QuestionnaireShapshotController(IClusterClient client, ILogger<QuestionnaireShapshotController> logger)
        {
            _client = client;
            _logger = logger;
        }


        [Route("{applicationId:int}"), HttpGet]
        public Task<Dictionary<Guid, object>> Get(int applicationId)
        {
            var grain = _client.GetGrain<IQuestionnaireManagerGrain>(applicationId);
            return grain.GetSnapshot();
        }
    }
}