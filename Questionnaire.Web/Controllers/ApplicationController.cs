﻿using System.Threading.Tasks;
using Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Orleans;
using Questionnaire.Web.Models;

namespace Questionnaire.Web.Controllers
{
    public class ApplicationController : Controller
    {
        private readonly IClusterClient _clusterClient;

        public ApplicationController(IClusterClient clusterClient)
        {
            _clusterClient = clusterClient;
        }

        public async Task<IActionResult> Index()
        {
            var grain = _clusterClient.GetGrain<IQuestionnaireListGrain>(0);
            var appList = await grain.GetAll();
            return View(new ApplicationListModel {Applications = appList});
        }

        public async Task<IActionResult> Edit(int id)
        {
            var grain = _clusterClient.GetGrain<IQuestionnaireManagerGrain>(id);
            var snapshot = await grain.GetSnapshot();

            return View(new ApplicationModel {Id = id, Snapshot = JsonConvert.SerializeObject(snapshot)});
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ApplicationModel model)
        {
            var grain = _clusterClient.GetGrain<IQuestionnaireManagerGrain>(model.Id);
            await grain.Set(model.ControlId, model.Value);
            var snapshot = await grain.GetSnapshot();
            return View("Edit", new ApplicationModel {Id = model.Id, Snapshot = JsonConvert.SerializeObject(snapshot)});
        }
    }
}