﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Orleans;

namespace Questionnaire.Web.Controllers
{
    [Route("api/[controller]")]
    public class QuestionnaireController : ControllerBase
    {
        private readonly IClusterClient _client;
        private readonly ILogger<QuestionnaireController> _logger;

        public QuestionnaireController(IClusterClient client, ILogger<QuestionnaireController> logger)
        {
            _client = client;
            _logger = logger;
        }

        [Route("{applicationId:int}/{controlId:Guid}"), HttpPost]
        public async Task Post(int applicationId, Guid controlId, string value)
        {
            var grain = _client.GetGrain<IQuestionnaireManagerGrain>(applicationId);
            await grain.Set(controlId, value);
        }

        [Route("{applicationId:int}/{controlId:Guid}"), HttpGet]
        public Task<object> Get(int applicationId, Guid controlId)
        {
            var grain = _client.GetGrain<IQuestionnaireManagerGrain>(applicationId);
            return grain.GetAnswer(controlId);
        }
    }
}