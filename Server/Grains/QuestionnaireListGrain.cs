﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using Domain.Interfaces;
using Orleans;

namespace Server.Grains
{
    public class QuestionnaireListGrain : Grain<HashSet<int>>, IQuestionnaireListGrain
    {
        public async Task Add(IQuestionnaireGrain q)
        {
            State.Add((int) q.GetPrimaryKeyLong());
            await WriteStateAsync();
        }

        public Task<int> GetMaxId()
        {
            return Task.FromResult(State.Count == 0 ? 0 : State.Max());
        }

        public Task<ImmutableArray<int>> GetAll()
        {
            return Task.FromResult(State.ToImmutableArray());
        }
    }
}