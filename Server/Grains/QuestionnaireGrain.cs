﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Interfaces;
using Microsoft.Extensions.Logging;
using Orleans;

namespace Server.Grains
{
    /// <summary>
    /// Orleans grain implementation class HelloGrain.
    /// </summary>
    public class QuestionnaireGrain : Grain<Dictionary<Guid, object>>, IQuestionnaireGrain
    {
        private readonly ILogger _logger;

        public override async Task OnActivateAsync()
        {
            var t = GrainFactory.GetGrain<IQuestionnaireListGrain>(0);
            await t.Add(this);
            await base.OnActivateAsync();
        }
        
        public QuestionnaireGrain(ILogger<QuestionnaireGrain> logger)
        {
            _logger = logger;
        }

        public async Task Set(Guid key, object value)
        {
            State[key] = value;
            _logger.LogInformation($"Questionnaire is updated with '{value}'");
            await WriteStateAsync();
        }

        public Task Remove(Guid key)
        {
            State.Remove(key);
            return Task.CompletedTask;
        }

        public Task<object> GetAnswer(Guid key)
        {
            return State.TryGetValue(key, out object val)
                ? Task.FromResult(val)
                : Task.FromResult((object) null);
        }

        public Task<int> GetCount() => Task.FromResult(State.Count);

        public Task<Dictionary<Guid, object>> GetSnapshot()
        {
            return Task.FromResult(State);
        }
    }
}