﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Interfaces;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Runtime;

namespace Server.Grains
{
    public class QuestionnaireManagerGrain : Grain, IQuestionnaireManagerGrain,
        IGrainWithIntegerKey
    {
        private readonly ILogger<QuestionnaireGrain> _logger;

        public QuestionnaireManagerGrain(ILogger<QuestionnaireGrain> logger)
        {
            _logger = logger;
        }

        public async Task Set(Guid key, object value)
        {
           var qId = this.GetPrimaryKeyLong();
            var questionarie = GrainFactory.GetGrain<IQuestionnaireGrain>(qId);
            await questionarie.Set(key, value);
            var prequalification = GrainFactory.GetGrain<IPrequalificationProcessorStatelessGrain>(0);
            var result = await prequalification.Evaluate(questionarie);
            _logger.Info($"For grain {qId} ({RuntimeIdentity}) Prequalification evaluated to {result.Status}");
            if (result.Status == PrequalificationStatus.Declined)
            {
                //Do something useful;
            }
        }

        public async Task Remove(Guid key)
        {
            var qId = this.GetPrimaryKeyLong();
            var questionarie = GrainFactory.GetGrain<IQuestionnaireGrain>(qId);
            await questionarie.Remove(key);
            var prequalification = GrainFactory.GetGrain<IPrequalificationProcessorStatelessGrain>(0);
            var result = await prequalification.Evaluate(questionarie);
            _logger.Info($"Evaluated to {result}");
            if (result.Status == PrequalificationStatus.Declined)
            {
                //Do something useful;
            }
        }

        public  Task<Dictionary<Guid, object>> GetSnapshot()
        {
            var qId = this.GetPrimaryKeyLong();
            var questionarie = GrainFactory.GetGrain<IQuestionnaireGrain>(qId);
            return questionarie.GetSnapshot();
        }

        public Task<object> GetAnswer(Guid controlId)
        {
            var qId = this.GetPrimaryKeyLong();
            var questionarie = GrainFactory.GetGrain<IQuestionnaireGrain>(qId);
            return questionarie.GetAnswer(controlId);
        }
    }
}