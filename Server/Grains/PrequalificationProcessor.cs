﻿using System;
using System.Threading.Tasks;
using Domain.Interfaces;
using Orleans;
using Orleans.Concurrency;

namespace Server.Grains
{
    [StatelessWorker]
    public class PrequalificationProcessorStatelessGrain : Grain, IPrequalificationProcessorStatelessGrain
    {
        private static readonly Guid SSNId = Guid.Parse("485821c4-20f6-4afb-8727-57bf7bef3135");

        public async Task<PrequalificationResult> Evaluate(IQuestionnaireGrain questionnaireGrain)
        {
            //the code gets a full copy of the questionnaire
            var snapshot = await questionnaireGrain.GetSnapshot();

            int answersCount = snapshot.Keys.Count;

            /*
             * May contain some complex logic, may also invoke external services, query db etc..
             * Note that all I/O operations MUST be asynchronious.
             *
             * The code like:
             * var result = httpClient.GetAsync(url).Wait();
             * IEnumerable<someData> dbResult = db.SearchByPatternAsync("Hello%").Wait();
             * may result performance issues.
             * 
             */
            await Task.Delay(TimeSpan.FromSeconds(0.1));

            PrequalificationStatus status;
            if (snapshot.ContainsKey(SSNId))
                status = PrequalificationStatus.Accepted;
            else if (answersCount < 5)
                status = PrequalificationStatus.Declined;
            else if (answersCount < 10)
                status = PrequalificationStatus.Underwriting;
            else
                status = PrequalificationStatus.Accepted;

            return new PrequalificationResult {Status = status};
        }
    }
}