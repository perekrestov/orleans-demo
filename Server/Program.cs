﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Loader;
using System.Threading.Tasks;
using Domain.Interfaces;
using McMaster.Extensions.CommandLineUtils;
using Microsoft.Extensions.Logging;
using Orleans.Clustering.Kubernetes;
using Orleans.Configuration;
using Orleans.Hosting;
using Orleans.Runtime;

namespace Server
{
    class Program
    {
        private static readonly TaskCompletionSource<int> TaskCompletionSource = new TaskCompletionSource<int>();
        private static ILogger<Program> _logger;

        static void Main(string[] args)
        {
            var app = new CommandLineApplication();

            app.HelpOption();
            var optionK8s = app.Option("-k|--kubernetes", "Runs the Silo in k8s", CommandOptionType.NoValue);
            var optionPostgresPersistence =
                app.Option("-p|--postgres", "Uses postgres grain storage", CommandOptionType.NoValue);
            app.OnExecute(async () =>
            {
                _logger = new LoggerFactory().AddConsole().CreateLogger<Program>();

                bool isSet = false;
                Console.CancelKeyPress += (s, e) =>
                {
                    e.Cancel = true;
                    isSet = true;
                    TaskCompletionSource.SetResult(0);
                };
                AssemblyLoadContext.Default.Unloading += ctx =>
                {
                    if (!isSet)
                    {
                        TaskCompletionSource.SetResult(0);
                    }
                };

                bool useKubernates = optionK8s.HasValue();
                bool usePostgres = optionPostgresPersistence.HasValue();

                var silo = await StartSilo(useKubernates, usePostgres);

                _logger.Info("Press Ctrl+C to stop silo.");
                await TaskCompletionSource.Task;

                _logger.Info("Stopping...");
                await silo.StopAsync();

                _logger.Info("Done.Exitting...");
            });
            app.Execute(args);
        }

        private static async Task<IPAddress> GetLocalIPAddress()
        {
            var host = await Dns.GetHostEntryAsync(Dns.GetHostName());
            var localIp = host
                .AddressList
                .FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork);

            if (localIp != null)
            {
                return localIp;
            }

            throw new Exception("No network adapters with an IPv4 address in the system!");
        }

        private static async Task<ISiloHost> StartSilo(bool useKubernates, bool usePostgres)
        {
            // define the cluster configuration
            var hostIpAddress =
                useKubernates
                    ? await GetLocalIPAddress()
                    : IPAddress.Loopback;

            var builder = new SiloHostBuilder()
                .ConfigureClustering(useKubernates)
                .ConfigureEndpoints(hostIpAddress, ClusterOptionConstants.DefaultSiloPort,
                    ClusterOptionConstants.DefaultGatewayPort);

            if (usePostgres)
            {
                string dbServer = useKubernates ? "postgres" : "127.0.0.1";
                builder.AddAdoNetGrainStorage("Default", options =>
                {
                    options.Invariant = "Npgsql";
                    options.ConnectionString =
                        $"Server={dbServer};Port=5432;Database=grains;User Id=postgres;Password=pwd;";
                    options.UseJsonFormat = true;
                });
            }
            else
            {
                builder.AddMemoryGrainStorageAsDefault();
            }

            builder.ConfigureLogging(logging => logging.AddConsole());

            var host = builder.Build();
            await host.StartAsync();
            return host;
        }
    }

    static class ConfigurationExtensions
    {
        public static ISiloHostBuilder ConfigureClustering(this ISiloHostBuilder builder, bool useKubernates)
        {
            const string ServiceId = "Server-Silo";
            if (useKubernates)
            {
                return builder
                    .Configure<ClusterOptions>(o =>
                    {
                        o.ClusterId = ClusterOptionConstants.ClusterId;
                        o.ServiceId = ServiceId;
                    })
                    .UseKubeMembership(opt =>
                    {
                        opt.CanCreateResources = true;
                        //opt.DropResourcesOnInit = true;
                    });
            }

            return builder.UseLocalhostClustering()
                /*
                 * note if you don't specify ServiceId, then you may get a very strange error while reading
                 * the first grain from the storage (postgres), e.g. cannot load assembly npgsql. It's because it
                 * cannot insert NULL into the ServiceId database column. I suppose it's because there is an issue
                 * in the exception handling logic in Orleans impl.
                */
                .Configure<ClusterOptions>(o => o.ServiceId = ServiceId);
        }
    }
}