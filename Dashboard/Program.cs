﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Loader;
using System.Threading.Tasks;
using McMaster.Extensions.CommandLineUtils;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Clustering.Kubernetes;
using Orleans.Configuration;
using Orleans.Hosting;
using Orleans.Runtime;

namespace Server
{
    class Program
    {
        const string DemoClusterId = "orleans-demo-cluster";
        const int LocalhostSiloPort = 11111;
        const int LocalhostGatewayPort = 30000;
        private static readonly TaskCompletionSource<int> TaskCompletionSource = new TaskCompletionSource<int>();
        private static ILogger<Program> _logger;

        static void Main(string[] args)
        {
            var app = new CommandLineApplication();

            app.HelpOption();
            var optionK8s = app.Option("-k|--kubernetes", "Runs the Silo in k8s", CommandOptionType.NoValue);
            app.OnExecute(async () =>
            {
                _logger = new LoggerFactory().AddConsole().CreateLogger<Program>();

                bool isSet = false;
                Console.CancelKeyPress += (s, e) =>
                {
                    e.Cancel = true;
                    isSet = true;
                    TaskCompletionSource.SetResult(0);
                };
                AssemblyLoadContext.Default.Unloading += ctx =>
                {
                    if (!isSet)
                    {
                        TaskCompletionSource.SetResult(0);
                    }
                };

                var silo = await StartSilo(useKubernates: optionK8s.HasValue());

                _logger.Info("Press Ctrl+C to stop silo.");
                await TaskCompletionSource.Task;

                _logger.Info("Stopping...");
                await silo.StopAsync();

                _logger.Info("Done.Exitting...");
            });
            app.Execute(args);
        }

        private static async Task<IPAddress> GetLocalIPAddress()
        {
            var host = await Dns.GetHostEntryAsync(Dns.GetHostName());
            var localIp = host
                .AddressList
                .FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork);

            if (localIp != null)
            {
                return localIp;
            }

            throw new Exception("No network adapters with an IPv4 address in the system!");
        }

        private static async Task<ISiloHost> StartSilo(bool useKubernates)
        {
            // define the cluster configuration
            var localAddress =
                useKubernates
                    ? await GetLocalIPAddress()
                    : IPAddress.Loopback;

            var builder = new SiloHostBuilder()
                .Configure<ClusterOptions>(o =>
                {
                    o.ClusterId = DemoClusterId;
                    o.ServiceId = "Dashboard-Service";
                })
                .Clustering(useKubernates)
                .ConfigureEndpoints(localAddress, LocalhostSiloPort, LocalhostGatewayPort)
                .UseDashboard(options => { })
                .ConfigureLogging(logging => logging.AddConsole());

            var host = builder.Build();
            await host.StartAsync();
            return host;
        }
    }

    static class ConfigurationExtensions
    {
        public static ISiloHostBuilder Clustering(this ISiloHostBuilder builder, bool useKubernates)
        {
            if (useKubernates)
            {
                return builder.UseKubeMembership(opt =>
                {
                    opt.CanCreateResources = false;
                    //opt.DropResourcesOnInit = true;
                });
            }

            return builder.UseLocalhostClustering();
        }
    }
}